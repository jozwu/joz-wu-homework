namespace HW1017
{
    public class Cashier
    {

        public IPay WayToPay { get; set; }

        public void ToPay(int e)
        {
            WayToPay?.Pay(e);
        }
    }
}
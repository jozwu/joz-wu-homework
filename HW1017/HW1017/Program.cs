﻿using System;

namespace HW1017
{
    class Program
    {
        static void Main(string[] args)
        {

            //Pay by cash
            var x = new Cashier
            {
                WayToPay = new Cash()
                
            };
            
            x.ToPay(100);
            
            //Pay by credit card
            var y = new Cashier
            {      
                WayToPay = new CreditCard()
            };
            
            y.ToPay(180);
            
            //Pay by ApplePay
            var z = new Cashier
            {
                WayToPay = new ApplePay()
            };
            
            z.ToPay(250);
            
            //Pay by LinePay
            var a = new Cashier
            {
                WayToPay = new LinePay()
            };
            
            a.ToPay(230);

        }
    }
}

//有一台收銀機
//可以用幾個方式結帳
//
//現金 
//刷卡
//Applepay
//Linepay
//
//用各種方式結帳時
//
//印出 已使用xxx（結帳方式）收款yyyy元
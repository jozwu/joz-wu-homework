using System;

namespace HW1017
{
    public class Cash : Payment, IPay
    {
        public override string Name => "Cash";

        public void Pay(int e)
        {
            Console.WriteLine($"已使用{Name}, 收款{e}元");                 
        }


    }
}
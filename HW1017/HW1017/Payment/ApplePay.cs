using System;

namespace HW1017
{
    public class ApplePay : Payment, IPay
    {
        public override string Name => "ApplePay";

        public void Pay(int e)
        {
            Console.WriteLine($"已使用{Name}, 收款{e}元");                 
        }
    }
}
using System;

namespace HW1017
{
    public class LinePay : Payment, IPay
    {
        public override string Name => "LinePay";

        public void Pay(int e)
        {
            Console.WriteLine($"已使用{Name}, 收款{e}元");                 
        }
    }
}
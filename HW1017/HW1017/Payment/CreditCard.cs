using System;

namespace HW1017
{
    public class CreditCard : Payment, IPay
    {
        public override string Name => "Credit Card";

        public void Pay(int e)
        {
            Console.WriteLine($"已使用{Name}, 收款{e}元");                 
        }
        
    }
}
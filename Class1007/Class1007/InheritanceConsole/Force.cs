using System;
using System.Collections.Generic;
using System.Linq;

namespace InheritanceConsole
{
    public class Force
    {
        public Force()
        {
            People.Add(new Farmer());
            
        }

        public List<Person> People { get; set; } = new List<Person>();
        public List<Building> Buildings { get; set; } = new List<Building>();


        public Farmer MyFarmer
        {
            get
            {
                var farmer = People.FirstOrDefault(x => x is Farmer);
                return farmer as Farmer;
            }
            


        }

        public void AllAttack()
        {
            People.ForEach(x => x.Attack());
        }

        public void CreatBuilding(BuildingTypeEnum type)
        {
            Buildings.Add(MyFarmer.CreatBuilding(type));

        }
        
        public void CreatPerson()
        {
            Buildings.ForEach(x => { People.Add(x.CreatPerson()); });

        }
      
        public void ListAllBuildings()
        {
            Console.WriteLine("有這些建築");
            Buildings.ForEach(x =>
                {
                    Console.WriteLine(x.GetType().Name);
                    
                }    
                
            );
            
        }
        
        public void ListAllPeople()
        {
            Console.WriteLine("有這些人");
            People.ForEach(x =>
                {
                    Console.WriteLine(x.GetType().Name);
                    
                }    
                
            );
            
        }

        public void ListProperties(bool listPerson)
        {
            Console.WriteLine(listPerson? "有這些人" : "有這些建築物");
            List<Unit> myUnits = new List<Unit>();

            if (listPerson)
            {
                People.ForEach(x => myUnits.Add(x));
            }
            else
            {
                Buildings.ForEach(x => myUnits.Add(x));
                
            }
            myUnits.ForEach(x => { Console.WriteLine(x.GetType().Name);});

        }
        
        


    }
}
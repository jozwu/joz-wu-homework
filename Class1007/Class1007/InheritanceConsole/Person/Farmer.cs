using System;
using System.Collections.Generic;
using System.Linq;

namespace InheritanceConsole
{
    public class Farmer : Person
    {
        public Farmer()
        {
        }

        public override void Attack() => Console.WriteLine("用鏟子攻擊");

        public Building CreatBuilding(BuildingTypeEnum type)
        {
            switch (type)
            {
                case BuildingTypeEnum.Farm:
                    return new Farm();
                    break;
                case BuildingTypeEnum.Church:
                    return new Church();
                    break;
                case BuildingTypeEnum.TrainingCenter:
                    return new TrainingCenter();
                    break;
                default:
                    return null;
            }
            
        }
    }
}
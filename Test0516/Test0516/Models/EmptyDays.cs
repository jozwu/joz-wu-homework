﻿using System;
using System.ComponentModel;

namespace Test0516.Models
{
    public enum EmumDays
    {
        [Description("星期ㄧ")]
        Monday = 1,
        [Description("星期二")]
        Tuesday = 2,
        [Description("星期三")]
        Wednesday = 3,
        [Description("星期四")]
        Thursday = 4,
        [Description("星期五")]
        Friday = 5,
        [Description("星期六")]
        Saturday = 6,
        [Description("星期日")]
        Sunday = 7,

    }
}

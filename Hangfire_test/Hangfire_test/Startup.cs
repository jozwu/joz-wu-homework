﻿using System;
using System.Threading.Tasks;
using Owin;
using Microsoft.Owin;
using Hangfire;
using Hangfire.MemoryStorage;



[assembly: OwinStartup(typeof(Hangfire_test.Startup))]

namespace Hangfire_test
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // 指定Hangfire使用記憶體儲存任務
            GlobalConfiguration.Configuration.UseMemoryStorage();
            // 啟用Hangfire的Dashboard
            app.UseHangfireDashboard("/h");
            // 啟用HanfireServer
            app.UseHangfireServer();

            app.Run(context =>
            {
                return context.Response.WriteAsync("Hello from ASP.NET Core!");
            });

            RecurringJob.AddOrUpdate(() => Console.WriteLine("Transparent!"), Cron.Minutely);

        }

}
}

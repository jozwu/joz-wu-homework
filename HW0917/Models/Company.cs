using System;
using System.Collections.Generic;
using System.Linq;

namespace HW0917
{
    public class Company {

        // public Company(){

        //     Employees.Add(new Person { Name = "Joz" , Job = JobEnum.Doctor });
        //     Employees.Add(new Person { Name = "Rob" , Job = JobEnum.Engineer });
        //     Employees.Add(new Person { Name = "Roman" , Job = JobEnum.Doctor });
        //     Employees.Add(new Person { Name = "Jack" , Job = JobEnum.Teacher });
        //     Employees.Add(new Person { Name = "Kelly" , Job = JobEnum.Chef });
        //     Employees.Add(new Person { Name = "Winnie" , Job = JobEnum.Teacher });
        //     Employees.Add(new Person { Name = "Chris" , Job = JobEnum.Chef });
        //     Employees.Add(new Person { Name = "Terry" , Job = JobEnum.Doctor });

        // }


        /// <summary>
        /// 公司名
        /// </summary>
        /// <value></value>
        public string Name { get; set; }



        /// <summary>
        /// 員工
        /// </summary>
        /// <value></value>
        public List<Person> Employees { get; set; } = new List<Person> {};

        public int EmployeesCount(JobEnum job){
            return Employees.Where (x => x.Job == job).Count ();   
        }

        public void EmployeesIntroduction(){

                foreach(var e in Employees){

                    Console.WriteLine(e.Introduction);
                }

        }


    }
    
}
using System;
using System.Linq;

namespace HW0917
{
    public class Person
    {
        /// <summary>
        /// 人名
        /// </summary>
        /// <value></value>
        public string Name {get; set;}

        /// <summary>
        /// 職業
        /// </summary>
        /// <value></value>
        public JobEnum Job {get; set;}

        /// <summary>
        /// 任職公司
        /// </summary>
        /// <value></value>
        public string Company {get; set;}

        /// <summary>
        /// 自我介紹
        /// </summary>
        /// <value></value>
        public string Introduction => $"我是{Name}，我的職業是{Job}，就職於{Company}";

    }
}
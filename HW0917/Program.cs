﻿using System;

namespace HW0917
{
    class Program
    {
        static void Main(string[] args)
        {
            var company = new Company { Name = "Starlux"};
            
            company.Employees.Add(new Person { Name = "Joz" , Job = JobEnum.Doctor , Company = company.Name });
            company.Employees.Add(new Person { Name = "Rob" , Job = JobEnum.Engineer , Company = company.Name });
            company.Employees.Add(new Person { Name = "Roman" , Job = JobEnum.Doctor , Company = company.Name });
            company.Employees.Add(new Person { Name = "Jack" , Job = JobEnum.Teacher , Company = company.Name });
            company.Employees.Add(new Person { Name = "Kelly" , Job = JobEnum.Chef , Company = company.Name });
            company.Employees.Add(new Person { Name = "Winnie" , Job = JobEnum.Teacher , Company = company.Name });
            company.Employees.Add(new Person { Name = "Chris" , Job = JobEnum.Chef , Company = company.Name });
            company.Employees.Add(new Person { Name = "Terry" , Job = JobEnum.Doctor , Company = company.Name });


            Console.WriteLine($@"
            {company.Name} has 
            {company.EmployeesCount(JobEnum.Engineer)} Engineers
            {company.EmployeesCount(JobEnum.Chef)} Chefs
            {company.EmployeesCount(JobEnum.Doctor)} Doctors
            {company.EmployeesCount(JobEnum.Teacher)} Teachers
            ");

            company.EmployeesIntroduction();
        }
    }
}

// 員工有人名和職業以及任職公司

// 職業有 
// 工程師
// 廚師
// 醫師
// 老師

// 公司有公司名字和員工

// 印出XXX公司有n名工程師，n名廚師，n名醫師，n名老師
// 並且讓每個員工自我介紹
// 印出 我是XXX，我的職業是XXX，就職於xxx
using System;

namespace HW1028
{
    public class Student
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }

        public void Introdution() => Console.WriteLine($"我是{Name}、學號為{ID}、{Age}歲、是{Gender}");            

    }
}
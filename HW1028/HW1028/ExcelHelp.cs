using System;
using System.Collections.Generic;

namespace HW1028
{
    static class ExcelHelp
        {


            public static List<Student> Students { get; set; } = new List<Student>();

            
            /// <summary>
            /// 增加學生
            /// </summary>
            /// <param name="student"></param>
            public static void AddStudent(Student student)
            {
                
                Students.Add(student);
                
            }
            
            /// <summary>
            /// 印出某位學生
            /// </summary>
            /// <param name="team"></param>
            /// <param name="e"></param>
            public static void PrintOneStudent(int e)
            {
                var student = Students[e];
                Console.WriteLine($"第{e}位學生是{student.Name}");
                
//                var ii = team.Student.Count;
//                
//                //例外狀況
//                if ( e > ii)
//                {
//                    return;
//                }
               
//                for (int i = 0; i <= team.Student.Count(); i++)
//                {
//                    if (i == e)
//                    {
//                        var query = team.Student.Skip(e - 1).Take(1);
//                        Console.WriteLine($"第{e}位學生是{query}");
//                    }
//                }
                
        
            }
    
            /// <summary>
            /// 印出所有學生的自我介紹
            /// </summary>
            /// <param name="cc"></param>
            public static void PrintAllStudent()
            {
                foreach (var e in Students)
                {
                    e.Introdution();
                }
        
            }
        }
}
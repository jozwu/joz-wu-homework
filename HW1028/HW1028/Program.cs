﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    namespace HW1028
    {
        class Program
        {
            static void Main(string[] args)
            {

                ExcelHelp.AddStudent(new Student{ID = 1001, Name ="Jack", Gender = "Male", Age = 16});
                ExcelHelp.AddStudent(new Student{ID = 1002, Name ="Bill", Gender = "Male", Age = 17});
                ExcelHelp.AddStudent(new Student{ID = 1003, Name ="Joanna", Gender = "Female", Age = 16});
                ExcelHelp.AddStudent(new Student{ID = 1004, Name ="Allen", Gender = "Male", Age = 20});
                ExcelHelp.AddStudent(new Student{ID = 1005, Name ="Boris", Gender = "Male", Age = 18});
                ExcelHelp.AddStudent(new Student{ID = 1006, Name ="Mai", Gender = "Female", Age = 15}); 
                ExcelHelp.PrintOneStudent(3);
                ExcelHelp.PrintAllStudent();
            }
  
        }
   
    }
    
    
    
    
    //設計一個ExcelHelp類別，有三個靜態方法
    //1. 新增一列學生資料  
    //2. 讀出第n列學生資料
    //3. 取得所有學生資料
    //
    //學生類別：
    //Id
    //Name
    //性別
    //年齡
    //
    //學生有一個public方法叫做自介
    //
    //執行自介時印出
    //我是xxx 學號為yyy xx歲 是男生（或女生）
    //
    //最後在program利用excelhelper取得所有學生資料
    //並且讓所有學生自介
    

﻿using System;

namespace Class1014
{
    class Program
    {
        static void Main(string[] args)
        {
//1.
//            var x = new Bike();
//            x.Movement();
//            var y = new Car();
//            y.Movement();
//            var z = new Airplane();
//            z.Movement();

//2.            
//            var x = new Chief_US();
//            var y = x.Cook();
//            Console.WriteLine(y.Name);

//3.         
//            var x = new CaptainAmerica();
//            var y = new IronMan();
//            var z = new Spiderman();
//            
//            Console.WriteLine($"{x.Name}, {x.SuperPower}" );
//            Console.WriteLine($"{y.Name}, {y.SuperPower}" );
//            Console.WriteLine($"{z.Name}, {z.SuperPower}" );

//4.
//            var x = new Pistol();
//            var y = new MachineGun();
//            var z = new RPG();
//            
//            Console.WriteLine($"{x.Name}, attack!");
//            x.Attack();
//            Console.WriteLine("----------------------");
//            Console.WriteLine($"{y.Name}, attack!");
//            y.Attack();
//            Console.WriteLine("----------------------");
//            Console.WriteLine($"{z.Name}, attack!");
//            z.Attack();
//            Console.WriteLine("----------------------");

//5.            
//            var x = new Soldier
//            {
//                MyItem = new MachineGun()
//            };
//
//            x.UseItem();

//6.                              
            var x = new People
            {
                Name ="Jack",
                UseTool = new PushNotice()
            };
            
            Console.WriteLine($"{x.Name}");
            x.ToContact();


        }
    }
    
    
    
}


//1.
//Bike		2 tires
//car 		4 tires
//Airplane  	engine
//
//2.
//Chief
//Us 
//Italy
//Taiwan
//
//
//3.
//Cook()  return food
//
//Food:
//hamburger
//Pasta
//meat rice
//
//
//4.
//Hero
//Superpower
//
//Captain American  Strength
//Ironman - Rich
//Spiderman - Spider Web
//
//5.
//Weapon 
//pistol          	1 bullet
//Machine gun 	3 bullet  -> for
//Rpg			1 rocket
//
//Attack()
//
//6.
//MINet
//
//Email
//Telex
//Message

















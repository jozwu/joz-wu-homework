using System;

namespace Class1014
{
    public class PushNotice : IContact
    {
        public string Name => "PushNotice";

        public void Use()
        {
            Console.WriteLine("Send PushNotice");
        }
    }
}
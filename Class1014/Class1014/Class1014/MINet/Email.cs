using System;

namespace Class1014
{
    public class Email : IContact
    {
        public string Name => "Email";

        public void Use()
        {
           Console.WriteLine("Send Email");
        }
    }
}
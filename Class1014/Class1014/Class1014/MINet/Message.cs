using System;

namespace Class1014
{
    public class Message : IContact
    {
        public string Name => "Message";

        public void Use()
        {
            Console.WriteLine("Send Message");
        }
    }
}
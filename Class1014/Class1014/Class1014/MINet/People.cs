namespace Class1014
{
    public class People
    {
        public string Name { get; set; }
        public IContact UseTool { get; set; }

        public void ToContact()
        {
            UseTool?.Use();
        }
       
    }
}
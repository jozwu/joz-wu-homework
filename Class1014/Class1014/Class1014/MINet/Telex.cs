using System;

namespace Class1014
{
    public class Telex : IContact
    {
        public string Name => "Telex";

        public void Use()
        {
            Console.WriteLine("Send Telex");
        }
    }
}
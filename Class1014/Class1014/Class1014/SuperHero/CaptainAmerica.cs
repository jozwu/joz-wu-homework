namespace Class1014
{
    public class CaptainAmerica : SuperHero
    {
        public override string Name => "Captain America";
        public override string SuperPower => "Strength";
    }
}
namespace Class1014
{
    public class Spiderman : SuperHero
    {
        public override string Name => "SpiderMan";
        public override string SuperPower => "Spider Web";
    }
}
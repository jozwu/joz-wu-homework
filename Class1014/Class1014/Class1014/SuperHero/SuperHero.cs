namespace Class1014
{
    public abstract class SuperHero
    {
        public virtual string Name { get; set; }
        public virtual string SuperPower { get; set; }
    }
}
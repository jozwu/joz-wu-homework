namespace Class1014
{
    public class IronMan : SuperHero
    {
        public override string Name => "IronMan";
        public override string SuperPower => "Rich";
    }
}
using System;

namespace Class1014
{
    public class Airplane : Vehicle
    {
        public override void Movement() => Console.WriteLine("用引擎起飛");
    }
}
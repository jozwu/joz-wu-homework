using System;

namespace Class1014
{
    public class MachineGun : Weapon, IItem
    {
        public override string Name => "Machine Gun";
        public void Use()
        {
            for ( int i = 0; i <= 2; i++)
            {
                Console.WriteLine("Shot one bullet");
            }

        }
    }
}
namespace Class1014
{
    public class Soldier 
    {
        public IItem MyItem { get; set; }
        
        public void UseItem()
        {
            MyItem?.Use();
        }

    }
}
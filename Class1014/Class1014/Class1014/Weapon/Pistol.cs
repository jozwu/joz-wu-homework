using System;

namespace Class1014
{
    public class Pistol : Weapon, IItem
    {
        public override string Name => "Pistol";
        public void Use()
        {
            Console.WriteLine("Shot one bullet");
        }
    }
}
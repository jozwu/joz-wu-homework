using System;

namespace Class1014
{
    public class RPG : Weapon, IItem
    {
        public override string Name => "RPG";
        public void Use()
        {
            Console.WriteLine("Shot one rocket");
        }
    }
}
using System;

namespace Class1014
{
    public abstract class Weapon
    {
        public virtual string Name { get; set; }
        public virtual void Attack() => Console.WriteLine("攻擊");
    }
}
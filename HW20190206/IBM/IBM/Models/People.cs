﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;


namespace IBM
{
    public class People : Controller
    {
        // <summary>
        /// 身高
        /// </summary>
        /// <value>The height.</value>/
        public Double Height { get; set; }
        // <summary>
        /// 體重
        /// </summary>
        /// <value>The weight.</value>/
        public Double Weight { get; set; }
        // <summary>
        /// BMI
        /// </summary>
        /// <value>The BMI.</value>/
        public Double ValueBMI => Weight / (Height/100) ^ 2;


    }
}

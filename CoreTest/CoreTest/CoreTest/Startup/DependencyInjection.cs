﻿using CoreTest.Service.Implement;
using CoreTest.Service.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace CoreTest.WebAPI.Startup
{
    public static class DependencyInjection
    {

        public static void AddDependencyInjection(this IServiceCollection services)
        {

            services.AddScoped<IHandlerService, HandlerService>();

        }

    }
}

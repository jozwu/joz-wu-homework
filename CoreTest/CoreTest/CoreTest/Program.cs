﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CoreTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
            var services = new ServiceCollection()
                                .AddLogging(config => config.AddConsole())
                                .BuildServiceProvider();

            var logger = services.GetRequiredService<ILogger<Program>>();

        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        private static IServiceCollection ConfigureServices(IServiceCollection services)
        {
            return services
              .AddLogging(builder =>
              {
                  builder.AddConsole();
              })
              .Configure<LoggerFilterOptions>(options =>
              {
                  options.MinLevel = LogLevel.Information;
              });
        }

    }
}

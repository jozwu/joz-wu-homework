using System;
using System.Collections.Generic;
using System.Linq;

namespace HW0928 {
    public class People {

        /// <summary>
        /// 生命值
        /// </summary>
        private int _Health;

        /// <summary>
        /// 戰鬥力
        /// </summary>
        private int _Fightpower;

        int _OriginalHealth;
        int _OriginalFightPower;

        //賽亞人變身的回合
        private int _SaiyanBeSuperRound;

        Random _Random = new Random ();

        /// <summary>
        /// 姓名
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// 種族
        /// </summary>
        /// <value></value>
        public RacesEnum Race { get; set; }

        /// <summary>
        /// 生命值
        /// </summary>
        /// <value></value>
        public int Health {
            get {
                return _Health;
            }
            set {
                _Health = value;
                if (_OriginalHealth == 0) {
                    _OriginalHealth = value;
                }

            }

        }

        /// <summary>
        /// 戰鬥力
        /// </summary>
        /// <value></value>
        public int FightPower {

            get {
                if (_Fightpower == 0) {
                    switch (Race) {
                        case RacesEnum.Human:
                            _OriginalFightPower = _Random.Next (50, 100);
                            _Fightpower = _OriginalFightPower;
                            break;
                        case RacesEnum.Saiyan:
                            _OriginalFightPower = _Random.Next (70, 90);
                            _Fightpower = _OriginalFightPower;
                            break;
                        case RacesEnum.SuperSaiyan:
                            _Fightpower = _OriginalFightPower * 3;
                            break;
                        case RacesEnum.Namekian:
                            _OriginalFightPower = _Random.Next (80, 120);
                            _Fightpower = _OriginalFightPower;
                            break;
                        default:
                            break;

                    }

                }
                return _Fightpower;
            }
            set {
                _Fightpower = value;
            }
        }

        /// <summary>
        /// 是否活著
        /// </summary>
        public bool IsAlive => Health != 0;

        /// <summary>
        /// 是否有啟動特殊能力
        /// </summary>
        public bool IsSpecialSkillActive = false;

        /// <summary>
        /// 特殊能力
        /// </summary>
        /// <param name="race"></param>
        public void SpecialSkill (People people, int round) {

            switch (people.Race) {

                case RacesEnum.Saiyan:
                    //賽亞人生命低於30%時，會變成超級賽亞人
                    //超級賽亞人的戰鬥力會變成原本的3倍

                    if ((!people.IsSpecialSkillActive) && (people.Health < (people._OriginalHealth * 0.3))) {
                        people.IsSpecialSkillActive = true;
                        people.Race = RacesEnum.SuperSaiyan;
                        people.FightPower *= 3;
                        _SaiyanBeSuperRound = round;
                        Console.WriteLine ($"{people.Name} 變身成為超級賽亞人 ");
                    }

                    break;
                case RacesEnum.SuperSaiyan:
                    //超級賽亞人發生戰鬥2次後會變回賽亞人，戰鬥力回到原本數值。
                    if (people.IsSpecialSkillActive && ((round - people._SaiyanBeSuperRound) > 2)) {

                        people.Race = RacesEnum.Saiyan;
                        people.FightPower /= 3;
                        Console.WriteLine ($"{people.Name} 恢復為賽亞人");

                    }

                    break;
                case RacesEnum.Namekian:
                    //納美克星人生命第一次低於30%時可以回覆生命到80%。
                    if ((!people.IsSpecialSkillActive) && (people.Health < (people._OriginalHealth * 0.3))) {
                        people.IsSpecialSkillActive = true;
                        // people.Health = Math.Round((people._OriginalHealth * 0.8), MidpointRounding.AwayFromZero);
                        people.Health = Convert.ToInt32 (people._OriginalHealth * 0.8);
                        Console.WriteLine ($"{people.Name} 生命值回復至{people.Health}");

                    }

                    break;
                default:

                    break;

            }

        }

    }
}
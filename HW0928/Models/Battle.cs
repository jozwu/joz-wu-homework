using System;

namespace HW0928 {
    public class Battle {
        public void Battles (People peopleA, People peopleB) {

            /// <summary>
            /// 回合數
            /// </summary>
            int round = 1;

            Console.WriteLine ($"{peopleA.Name} ({peopleA.Race})，生命值{peopleA.Health} 攻擊力{peopleA.FightPower} ");
            Console.WriteLine ($"{peopleB.Name} ({peopleB.Race})，生命值{peopleB.Health} 攻擊力{peopleB.FightPower} ");

            //若雙方戰鬥力相同，則跳出戰鬥
            if (peopleA.FightPower == peopleB.FightPower) {

                Console.WriteLine ($"{peopleA.Name} 與 {peopleB.Name} 戰鬥力相同，故結束戰鬥  ");
                return;

            }

            while ((peopleA.IsAlive == true) && (peopleB.IsAlive == true)) {

                Fights (peopleA, peopleB, round);

                // 檢查雙方是否死亡，若一方死亡，即結束戰鬥
                if (!peopleA.IsAlive || !peopleB.IsAlive) {

                    if (!peopleA.IsAlive) {

                        Console.WriteLine ($"{peopleA.Name}死亡，故結束戰鬥 共{round}回合 ");

                    } else {

                        Console.WriteLine ($"{peopleB.Name}死亡，故結束戰鬥 共{round}回合 ");

                    }

                    break;
                }

                round++;
            }

        }

        /// <summary>
        /// 戰鬥傷害計算
        /// </summary>
        /// <param name="peopleA"></param>
        /// <param name="peopleB"></param>
        /// <param name="round"></param>
        public void Fights (People peopleA, People peopleB, int round) {

            int harm = 0;

            if (round % 2 != 0) {

                if (peopleA.FightPower > peopleB.FightPower) {

                    harm = peopleA.FightPower - peopleB.FightPower;

                    if (peopleB.Health - harm <= 0) {

                        peopleB.Health = 0;

                    } else {

                        peopleB.Health -= harm;

                    }

                    //印出每回合戰鬥狀況
                    StatusOfBattle (peopleA, peopleB, peopleB, harm, round);
                    //檢查是否須啟動特殊能力
                    peopleB.SpecialSkill (peopleA, round);
                    peopleB.SpecialSkill (peopleB, round);

                } else {

                    harm = peopleB.FightPower - peopleA.FightPower;

                    if (peopleA.Health - harm <= 0) {

                        peopleA.Health = 0;

                    } else {

                        peopleA.Health -= harm;

                    }

                    //印出每回合戰鬥狀況
                    StatusOfBattle (peopleA, peopleB, peopleA, harm, round);
                    //檢查是否須啟動特殊能力
                    peopleB.SpecialSkill (peopleA, round);
                    peopleB.SpecialSkill (peopleB, round);

                }

            } else {

                if (peopleA.FightPower < peopleB.FightPower) {

                    harm = peopleB.FightPower - peopleA.FightPower;

                    if (peopleA.Health - harm <= 0) {

                        peopleA.Health = 0;

                    } else {

                        peopleA.Health -= harm;

                    }

                    //印出每回合戰鬥狀況
                    StatusOfBattle (peopleB, peopleA, peopleA, harm, round);
                    //檢查是否須啟動特殊能力
                    peopleB.SpecialSkill (peopleA, round);
                    peopleB.SpecialSkill (peopleB, round);

                } else {

                    harm = peopleA.FightPower - peopleB.FightPower;

                    if (peopleB.Health - harm <= 0) {

                        peopleB.Health = 0;

                    } else {

                        peopleB.Health -= harm;

                    }

                    //印出每回合戰鬥狀況
                    StatusOfBattle (peopleB, peopleA, peopleB, harm, round);
                    //檢查是否須啟動特殊能力
                    peopleB.SpecialSkill (peopleA, round);
                    peopleB.SpecialSkill (peopleB, round);

                }

            }

        }

        /// <summary>
        /// 印出每回合戰鬥狀況
        /// </summary>
        /// <param name="peopleA"></param>
        /// <param name="peopleB"></param>
        public void StatusOfBattle (People peopleA, People peopleB, People peopleHarm, int harm, int round) {

            Console.WriteLine ($"第{round}回合 {peopleA.Name}({peopleA.Race},{peopleA.FightPower}) 攻擊 {peopleB.Name}({peopleB.Race},{peopleB.FightPower})，造成{peopleHarm.Name} {harm}傷害 - {peopleA.Name}生命值{peopleA.Health} // {peopleB.Name} 生命值{peopleB.Health}  ");

        }

    }

}
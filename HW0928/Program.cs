﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HW0928
{
    class Program
    {
        static void Main(string[] args)
        {

            var warriorA = new People { Name = "Joz", Race = RacesEnum.Saiyan, Health = 1000 };
            var warriorB = new People { Name = "Hotch", Race = RacesEnum.Human, Health = 1000 };
            var warriorC = new People { Name = "Badman", Race = RacesEnum.Namekian, Health = 1000 };
           
            var battle1 = new Battle{};
            // 第一局 地球人對那美克星人
            battle1.Battles(warriorB, warriorC);
            // 第二局 地球人對賽亞人
            battle1.Battles(warriorB, warriorA);
            // 第三局 賽亞人對那美克星人
            battle1.Battles(warriorA, warriorC);

        }
    }
}



// 七龍珠-----------------------

// 人有人名，戰鬥力，生命值，種族，是否活著
// 人可以攻擊別人，戰鬥力低者損失戰鬥力差額的生命值 --finish 

// 生命值=0時，人死亡。     --finish 

// 種族有           --finish 
// 賽亞人
// 超級賽亞人
// 那美克星人
// 地球人

// 賽亞人生命低於30%時 會變成超級賽亞人                     --finish
// 超級賽亞人的戰鬥力會變成原本的3倍。                      --finish
// 超級賽亞人發生戰鬥2次後會變回賽亞人，戰鬥力回到原本數值。    --finish
// 納美克星人生命第一次低於30%時可以回覆生命到80%。           --finish

// 執行數場對戰。（a攻擊玩換b,直到一方死亡為止。

// 第一局 地球人對那美克星人
// 第二局 地球人對賽亞人
// 第三局 賽亞人對那美克星人

// 所有人開場生命1000               --finish
// 地球人開場戰鬥力50～100亂數       --finish
// 納美克星人開場戰鬥力80～120亂數    --finish
// 賽亞人開場戰鬥力70～90亂數        --finish
<Query Kind="Program" />

void Main()
{
	var pilot_a = new Crew { Name = "Eric", Job = Position.Captain };
	var pilot_b = new Crew { Name = "Joz", Job = Position.Pilot };
	var ca1 = new Crew { Name = "Amy", Job = Position.Cabin };
	var ca2 = new Crew { Name = "Batty", Job = Position.Cabin };
	var ca3 = new Crew { Name = "Cathy", Job = Position.Cabin };
	var ca4 = new Crew { Name = "Dolly", Job = Position.Cabin };
	var ca5 = new Crew { Name = "Ellie", Job = Position.Cabin };
	
	var x = new Flight{
		AirlineCompany = "CI" ,
		FlightNo ="0123"
	};
	
	x.Crews.AddRange(new List<Crew>{pilot_a,pilot_b,ca1,ca2,ca3,ca4,ca5});
	//x.Dump();
	x.Flight_Detail();
	
}

public class Flight {
	
	public string AirlineCompany {get; set;}
	public string FlightNo {get; set;}
	public List<Crew> Crews {get; set;}  = new List<Crew>{};
	public string Pilot1 {
		get{
			string xx = "";
			foreach(var e in Crews){
				if(e.Job == Position.Captain ){
					xx = e.Name ;
				}
			
			}
			return xx;
		
		}
	}
	public string Pilot2 {
		get{
			string yy = "";
			foreach(var e in Crews){
				if(e.Job == Position.Pilot ){
					yy = e.Name ;
				}
			
			}
			return yy;
		
		}
	
	
	}
	public int Cabins {
		get{
			int zz = 0;
			foreach(var e in Crews){
				if(e.Job == Position.Cabin ){
					zz ++;
				}
			
			}
			return zz;
		}	
	}
	
	public void Flight_Detail(){
	
		$"{AirlineCompany}{FlightNo} 機長為{Pilot1},副機師為{Pilot2},組員共{Cabins}位".Dump();
			

	}
	
}


public class Crew{
	public string Name {get; set;}
	public Position Job {get; set;}

}


public enum Position {
	Captain,
	Pilot,
	Cabin
}


// Define other methods and classes here

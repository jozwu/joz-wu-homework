﻿using System;

namespace SelfHW0922
{
    class Program
    {
        static void Main(string[] args)
        {
            var team01 = new Team { Name = "Surfer"};

            team01.Players.Add( new Player { Name = "Williams", Position = PositionEnum.PG, Salary = 50000 });
            team01.Players.Add( new Player { Name = "Morris", Position = PositionEnum.SG, Salary = 45000 });
            team01.Players.Add( new Player { Name = "Asik", Position = PositionEnum.C, Salary = 200000 });
            team01.Players.Add( new Player { Name = "Jackson", Position = PositionEnum.PG, Salary = 35000 });
            team01.Players.Add( new Player { Name = "Toawer", Position = PositionEnum.C, Salary = 100000 });
            team01.Players.Add( new Player { Name = "Davis", Position = PositionEnum.PF, Salary = 90000 });
            team01.Players.Add( new Player { Name = "Baldwin", Position = PositionEnum.SF, Salary = 280000 });
            team01.Players.Add( new Player { Name = "Staple", Position = PositionEnum.PG, Salary = 88000 });
            team01.Players.Add( new Player { Name = "Lin", Position = PositionEnum.SG, Salary = 30000 });
            team01.Players.Add( new Player { Name = "Curry", Position = PositionEnum.PF, Salary = 150000 });

            
            
            
            Console.WriteLine($@"
            Team {team01.Name} has 
            {team01.EachPositionCount(PositionEnum.PG)} PG
            {team01.EachPositionCount(PositionEnum.SG)} SG
            {team01.EachPositionCount(PositionEnum.SF)} SF
            {team01.EachPositionCount(PositionEnum.PF)} PF
            {team01.EachPositionCount(PositionEnum.C)} C
            Total Salary: {team01.TotalSalary(team01.Players)}
            ");


        }
    }
}

using System;
using System.Linq;


namespace SelfHW0922
{
    public class Player
    {
        /// <summary>
        /// 人名
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// 位置
        /// </summary>
        /// <value></value>
        public PositionEnum Position { get; set; }

        /// <summary>
        /// 年薪
        /// </summary>
        /// <value></value>
        public int Salary { get; set; }

    }
}
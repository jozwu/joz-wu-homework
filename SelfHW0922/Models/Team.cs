using System;
using System.Collections.Generic;
using System.Linq;

namespace SelfHW0922
{
    public class Team
    {
        
        /// <summary>
        /// 隊名
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// 球員
        /// </summary>
        /// <value></value>
        public List<Player> Players { get; set; } = new List<Player> {};

        /// <summary>
        /// 球員總薪資
        /// </summary>
        /// <returns></returns>
        public int TotalSalary(List<Player> players){
            int _totalsalary = 0;
            
                foreach(var e in players){

                    _totalsalary += e.Salary;
                    
                }

                return _totalsalary;

        }
        
        /// <summary>
        /// 各位子人數統計
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public int EachPositionCount (PositionEnum position){

                return Players.Where ( x => x.Position == position).Count ();
                
        }



    }
}
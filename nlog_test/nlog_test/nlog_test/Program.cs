﻿using System;
using NLog;

namespace nlog_test
{
    class MainClass
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void Main(string[] args)
        {

            //Console.ReadLine();
            //logger.Trace("我是追蹤:Trace");
            //logger.Debug("我是偵錯:Debug");
            //logger.Info("我是資訊:Info");
            //logger.Warn("我是警告:Warn");
            //logger.Error("我是錯誤:error");
            //logger.Fatal("我是致命錯誤:Fatal");


            DateTime tNow = DateTime.Now;
            DateTime birthdate = new DateTime(2018, 10, 10);
            TimeSpan result = tNow - birthdate;
            double s = result.TotalDays / 354;
            logger.Trace("Time: " + result + " years ");

            Console.WriteLine("Hello World!");
        }
    }
}

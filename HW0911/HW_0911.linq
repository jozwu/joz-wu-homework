<Query Kind="Program" />

void Main()
{
	//一開始的農民
	var jack = new Man{ MID = 1, Job = Career.Farmer};
	
	//僧侶
//	var smith = new Man{ MID = 4, Job = Career.Monk};
//	var allen = new Man{ MID = 5, Job = Career.Monk};
//	var joseph = new Man{ MID = 6, Job = Career.Monk};
//	var alex = new Man{ MID = 7, Job = Career.Monk};
//	var rob = new Man{ MID = 8, Job = Career.Monk};
//
//	//騎士
//	var mark = new Man{ MID = 9, Job = Career.Knight};
//	var stan = new Man{ MID = 10, Job = Career.Knight};
//	var murry = new Man{ MID = 11, Job = Career.Knight};
//	var parker = new Man{ MID = 12, Job = Career.Knight};
//	var duncan = new Man{ MID = 13, Job = Career.Knight};
//	var manu = new Man{ MID = 14, Job = Career.Knight};
//	var paul = new Man{ MID = 15, Job = Career.Knight};
//	
	//農民jack蓋教堂church1
	var church1 = jack.BuildHouse(Building.Church);
	
	//農民jack蓋農場farm1
	var farm1 = jack.BuildHouse(Building.Farm);
	
	//農民jack蓋訓練所training1
	var training1 = jack.BuildHouse(Building.Training);
	
	//建築物church1生產僧侶frank
	var frank = church1.ProduceMan(Career.Monk);

	//建築物farm1生產農民bill
	var bill = farm1.ProduceMan(Career.Farmer);

	//建築物training1生產騎士jerry
	var jerry = training1.ProduceMan(Career.Knight);
	
	//農民bill蓋訓練所training2
	var training2 = bill.BuildHouse(Building.Training);
	
	//農民bill蓋農場farm2
	var farm2 = bill.BuildHouse(Building.Farm);
	
	//建築物training2生產騎士terry
	var terry = training2.ProduceMan(Career.Knight);
	
	var xxxxx = new Army{};
	xxxxx.People.AddRange(new List<Man>{jack, frank, bill, jerry, terry});
	xxxxx.Houses.AddRange(new List<House>{church1, farm1, training1, training2, farm2});

	
	//xxxxx.Dump();
	xxxxx.Army_detail();
	//xxxxx.Count(Career.Knight).Dump();

	
	
	
}


public class Army{
	public List<Man> People {get; set;} = new List<Man>{};
	public List<House> Houses {get; set;} = new List<House>{};

//	public int Count(Career career){
//		
//		int count = 0;
//		
//		foreach(var e in People ){
//				if(e.Job == career){			
//					count++;
//				}
//		}
//	
//		return count;
//	}

	//統計人數
	public int PeopleCount(List<Man> people ,Career career){
		
			int count = 0;
			
			foreach(var e in people ){
					if(e.Job == career){			
						count++;
					}
			}
	
		return count;
	}
	
	//統計建築物
	public int HouseCount(List<House> houses ,Building building){
		
			int count = 0;
			
			foreach(var e in houses ){
					if(e.Type == building){			
						count++;
					}
			}
	
		return count;
	}

	public void Army_detail(){
	
		int farmerCount = PeopleCount(People,Career.Farmer);
		int monkCount = PeopleCount(People,Career.Monk);
		int knightCount = PeopleCount(People,Career.Knight);
		int farmCountCount = HouseCount(Houses,Building.Farm);
		int churchCount = HouseCount(Houses,Building.Church);
		int trainingCount = HouseCount(Houses,Building.Training);
		
		$"軍團一共有 {farmerCount}個農民 {monkCount}個僧侶 {knightCount}個騎士 {farmCountCount}棟農舍 {churchCount}棟教堂 {trainingCount}棟訓練所".Dump();
		
	}

}





public class Man{
	public int MID {get; set;}
	public Career Job {get; set;}
	
	
	public House BuildHouse(Building building){
		if(Job == Career.Farmer){		
		
//			if(building == Building.Farm){		
//				return new House(){ BID = 1, Type = Building.Farm};
//			}
//			
//			if(building == Building.Church){
//				return new House(){ BID = 1, Type = Building.Church};
//			}
//			
//			if(building == Building.Training){
//				return new House(){ BID = 1, Type = Building.Training};
//			}
			
			switch(building){
			
				case Building.Farm:
                	return new House(){ BID = 1, Type = Building.Farm};
                
            	case Building.Church:
                	return new House(){ BID = 1, Type = Building.Church};
				
				case Building.Training:
                	return new House(){ BID = 1, Type = Building.Training};
			
			}
			
		}
	
		return new House{};
	}
	

}

public class House{
	public int BID {get; set;}
	public Building Type {get; set;}
	
	public Man ProduceMan(Career career){
	
//		if(Type == Building.Farm){
//			return new Man(){ MID = 1, Job = Career.Farmer};	
//		}
//		if(Type == Building.Church){
//			return new Man(){ MID = 1, Job = Career.Monk};	
//		}
//		if(Type == Building.Training){
//			return new Man(){ MID = 1, Job = Career.Knight};	
//		}
		
			switch(Type){
			
				case Building.Farm:
                	return new Man(){ MID = 1, Job = Career.Farmer};
                
            	case Building.Church:
                	return new Man(){ MID = 1, Job = Career.Monk};
				
				case Building.Training:
                	return new Man(){ MID = 1, Job = Career.Knight};
			
			}
		
		return new Man();
	}


}

public enum Career {
	Farmer,
	Monk,
	Knight
}

public enum Building {
	Farm,
	Church,
	Training
}


//軍團:
//- 可以有很多個人
//- 可以有很多個建築物
//- 一開始只有一個農民
//
// 人:
// - 有職業區分 農民/僧侶/騎士
// - 有ID
//
// 建築物:
// - 有ID
// - 有區分 農舍/教堂/訓練所
//
// 農民:
// -可以為軍團蓋建築物 蓋的時候選擇要蓋農舍/教堂/訓練所
// 
// 農舍:
// -可以為軍團生產農民
//
// 教堂:
// -可以為軍團生產僧侶
//
// 訓練所:
// -可以為軍團生產騎士
//
//印出 軍團一共有 a個農民 b個僧侶 c個騎士 x棟農舍 y棟教堂 z 棟訓練所

// Define other methods and classes here